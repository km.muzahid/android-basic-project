package com.example.productmanagement.ui.addProducts

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.productmanagement.R
import com.example.productmanagement.ui.Database

class AddProductFragment : Fragment() {

    private lateinit var addProductModelViewModel: AddProductModelViewModel

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        addProductModelViewModel =
                ViewModelProvider(this).get(AddProductModelViewModel::class.java)

        val root = inflater.inflate(R.layout.fragment_add_products, container, false)
        val spinnerCategory = root.findViewById<Spinner>(R.id.spinner_product_category)
        val btnAddProducts = root.findViewById<Button>(R.id.btn_add_products)
        initialize(root, spinnerCategory)
        btnAddProducts.setOnClickListener {
            addProducts(root, spinnerCategory)
        }
        return root
    }

    private fun initialize(root:View, spinnerCategory: Spinner) {
        ArrayAdapter.createFromResource(
            root.context,
            R.array.products_categories_array,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinnerCategory.adapter = adapter
        }
    }

    private fun addProducts(root: View, spinnerCategory: Spinner ){
        val editTextPrice = root.findViewById<EditText>(R.id.editText_price)
        val editTextProductName = root.findViewById<EditText>(R.id.editText_name)
            val data = ArrayList<String>()
            data.add(editTextProductName.text.toString())
            data.add(spinnerCategory.selectedItem.toString())
            data.add(editTextPrice.text.toString())
            Database().addProducts(data)
        editTextPrice.setText("")
        editTextProductName.setText("")
        Toast.makeText(root.context, "Product has been saved", Toast.LENGTH_SHORT).show()
    }
}