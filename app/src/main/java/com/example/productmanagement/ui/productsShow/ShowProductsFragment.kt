package com.example.productmanagement.ui.productsShow

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.productmanagement.R
import com.example.productmanagement.ui.Database

class ShowProductsFragment : Fragment() {

    private lateinit var showProductsViewModel: ShowProductsViewModel

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        showProductsViewModel =
                ViewModelProvider(this).get(ShowProductsViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_products, container, false)
        val recyclerProductsShow = root.findViewById<RecyclerView>(R.id.recycler_products)
        recyclerProductsShow.layoutManager = LinearLayoutManager(root.context)
        recyclerProductsShow.adapter = CustomAdapter(Database().productsList())
        return root
    }
}