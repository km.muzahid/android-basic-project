package com.example.productmanagement.ui.productsShow

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.productmanagement.R


class CustomAdapter(private val dataSet: ArrayList<ArrayList<String>>) :
    RecyclerView.Adapter<CustomAdapter.ViewHolder>() {

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val textViewProductName: TextView
        val textViewCategory: TextView
        val textViewPrice: TextView
        init {
            textViewProductName = view.findViewById(R.id.textView_product_name)
            textViewCategory = view.findViewById(R.id.textView_category)
            textViewPrice = view.findViewById(R.id.textView_price)
        }
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.text_row_item, viewGroup, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        viewHolder.textViewProductName.text = dataSet[position][0]
        viewHolder.textViewCategory.text = dataSet[position][1]
        viewHolder.textViewPrice.text = "$ "+dataSet[position][2]
    }
    override fun getItemCount() = dataSet.size

}
