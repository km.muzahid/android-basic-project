package com.example.productmanagement.ui

class Database {
    //Remove companion object and initialize database components
    companion object{
        private val products = ArrayList<ArrayList<String>>()
    }
    fun addProducts(newProduct:ArrayList<String>) {
        products.add(newProduct)
    }
    fun productsList() = products
}